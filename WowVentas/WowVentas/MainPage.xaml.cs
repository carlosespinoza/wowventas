﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WowVentas.Vistas;
using Xamarin.Forms;

namespace WowVentas
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            btnProductos.Clicked += (sender, e) => Navigation.PushAsync(new viewProductos());
            btnNuevaVenta.Clicked += (sender, e) => Navigation.PushAsync(new viewNuevaVenta());
            btnEstadisticas.Clicked += (sender, e) => Navigation.PushAsync(new viewEstadistica());
            btnCreditos.Clicked += (sender, e) => Navigation.PushAsync(new ViewCreditos());
        }


    }
}
