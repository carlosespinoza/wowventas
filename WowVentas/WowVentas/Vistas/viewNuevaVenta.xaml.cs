﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WowVentas.Entidades;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace WowVentas.Vistas
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class viewNuevaVenta : ContentPage
	{
        Repositorio<Venta> repositorioVentas;
        Repositorio<Producto> repositorioProductos;
        Venta venta;
		public viewNuevaVenta ()
		{
			InitializeComponent ();
            venta = new Venta();
            repositorioProductos = new Repositorio<Producto>();
            repositorioVentas = new Repositorio<Venta>();
            pkrProductos.ItemsSource = repositorioProductos.Read;
            grdContenedor.BindingContext = venta;
            btnAgregar.Clicked += (sender, e) =>
            {
                if (pkrProductos.SelectedItem != null)
                {
                    venta.Items.Add(new ItemVenta(int.Parse(txbCantidad.Text), pkrProductos.SelectedItem as Producto));
                    lstItems.ItemsSource = null;
                    lstItems.ItemsSource = venta.Items;
                    lblTotal.Text = String.Format("Total: ${0}", venta.Total);
                }
            };
            btnGuadar.Clicked += (sender, e) =>
            {
                if (string.IsNullOrEmpty(venta.Vendedor))
                {
                    DisplayAlert("Aviso", "Te falta el vendedor", "Ok");
                    return;
                }
                if (string.IsNullOrEmpty(venta.Cliente))
                {
                    DisplayAlert("Aviso", "Te falta el cliente", "Ok");
                    return;
                }
                if (repositorioVentas.Create(venta) != null)
                {
                    DisplayAlert("Aviso", "Venta generada correctamente", "Ok");
                    Navigation.PopAsync();
                }
                else
                {
                    DisplayAlert("Error", "Error al generar la venta", "Ok");
                }
            };
		}
	}
}