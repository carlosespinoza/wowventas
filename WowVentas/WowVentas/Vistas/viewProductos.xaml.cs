﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WowVentas.Entidades;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace WowVentas.Vistas
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class viewProductos : ContentPage
    {
        Repositorio<Producto> repositorio;
        bool esNuevo;
        public viewProductos()
        {
            try
            {
                InitializeComponent();
                repositorio = new Repositorio<Producto>();
                btnGuardar.Clicked += BtnGuardar_Clicked;
                btnEliminar.Clicked += BtnEliminar_Clicked;
                lstProductos.ItemSelected += LstProductos_ItemSelected;
                ActualizarLista();
            }
            catch (Exception ex)
            {
                DisplayAlert("Error: ", ex.Message, "Ok");
            }
            
        }

        private void LstProductos_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (lstProductos.SelectedItem != null)
            {
                esNuevo = false;
                grdContenedor.BindingContext = lstProductos.SelectedItem as Producto;
            }
        }

        private void BtnEliminar_Clicked(object sender, EventArgs e)
        {
            if (lstProductos.SelectedItem != null)
            {
                if (DisplayAlert("Confirmación", "Realmente desea eliminar este producto?", "Sí", "No").Result)
                {
                    if (repositorio.Delete((lstProductos.SelectedItem as Producto).Id))
                    {
                        DisplayAlert("Aviso", "Producto Eliminado correctamente", "Ok");
                        ActualizarLista();
                    }
                    else
                    {
                        DisplayAlert("Error", "No se pudo eliminar el producto", "Ok");
                    }
                }
            }
        }

        private void BtnGuardar_Clicked(object sender, EventArgs e)
        {
            if (esNuevo)
            {
                if (repositorio.Create(grdContenedor.BindingContext as Producto) != null)
                {
                    DisplayAlert("Aviso", "Producto Almacenado correctamente", "Ok");
                    ActualizarLista();
                }
                else
                {
                    DisplayAlert("Error", "No se pudo almacenar el producto", "Ok");
                }
            }
            else
            {
                if (repositorio.Update(grdContenedor.BindingContext as Producto) != null)
                {
                    DisplayAlert("Aviso", "Producto Almacenado correctamente", "Ok");
                    ActualizarLista();
                }
                else
                {
                    DisplayAlert("Error", "No se pudo almacenar el producto", "Ok");
                }
            }
        }

        private void ActualizarLista()
        {
            grdContenedor.BindingContext = new Producto();
            esNuevo = true;
            lstProductos.ItemsSource = null;
            lstProductos.ItemsSource = repositorio.Read;
        }
    }
}