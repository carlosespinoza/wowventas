﻿using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WowVentas.Entidades;
using WowVentas.Modelos;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace WowVentas.Vistas
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class viewEstadistica : ContentPage
	{
        int num = 0;
        Repositorio<Producto> repositorioProductos;
        Repositorio<Venta> repositorioVentas;
        List<Venta> ventas;
        List<Producto> productos;
		public viewEstadistica ()
		{
			InitializeComponent ();
            repositorioVentas = new Repositorio<Venta>();
            repositorioProductos = new Repositorio<Producto>();
            ventas = repositorioVentas.Read;
            productos = repositorioProductos.Read;
            List<Estadistica> datos = new List<Estadistica>();
            foreach (var producto in productos)
            {
                datos.Add(CalcularVenta(producto.Nombre));
            }

            PlotModel model = new PlotModel();
            CategoryAxis ejeX = new CategoryAxis();
            ejeX.Position = AxisPosition.Bottom;

            CategoryAxis ejeY = new CategoryAxis();
            ejeY.Position = AxisPosition.Left;

            model.Axes.Add(ejeY);
            model.Axes.Add(ejeX);

            model.Title = "Ventas";
            ColumnSeries columas = new ColumnSeries();
            columas.Title = "Monto";
            ColumnItem item;
            foreach (var dato in datos)
            {
                item = new ColumnItem();
                item.CategoryIndex = dato.Numero;
                item.Value = dato.Monto;
                columas.Items.Add(item);
            }
            model.Series.Add(columas);
            chrEstadistica.Model = model;
            lstEstadistica.ItemsSource = datos;
		}

        private Estadistica CalcularVenta(string nombre)
        {
            int cantidad = 0;
            float total = 0;
            foreach (var venta in ventas)
            {
                foreach (var producto in venta.Items)
                {
                    if (producto.Producto.Nombre == nombre)
                    {
                        cantidad += producto.Cantidad;
                        total += producto.Cantidad * producto.Producto.Costo;
                        
                    }
                }
            }
            return new Estadistica()
            {
                Numero = num++,
                Producto = nombre,
                Cantidad = cantidad,
                Monto = total
            };
        }
    }
}