﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;
using WowVentas.Entidades;

namespace WowVentas
{
    public class Repositorio<T> where T:BaseDTO
    {
        private MongoClient client;
        private IMongoDatabase db;
        public Repositorio()
        {
            client = new MongoClient(new MongoUrl(@"mongodb://PruebaMQTT:Halo4ygokugt@cluster0-shard-00-00-ad0gd.mongodb.net:27017,cluster0-shard-00-01-ad0gd.mongodb.net:27017,cluster0-shard-00-02-ad0gd.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true"));
            db = client.GetDatabase("wowventas");
        }
        private IMongoCollection<T> Collection()
        {
            return db.GetCollection<T>(typeof(T).Name);
        }
        public List<T> Read => Collection().AsQueryable<T>().ToList();

        public T Create(T entidad)
        {
            entidad.Id = ObjectId.GenerateNewId();
            try
            {
                Collection().InsertOne(entidad);
                return entidad;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public bool Delete(ObjectId id)
        {
            try
            {
                return Collection().DeleteOne(m => m.Id == id).DeletedCount == 1;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public T SearchById(ObjectId id)
        {
            try
            {
                return Collection().Find(e => e.Id == id).SingleOrDefault();
            }
            catch (Exception)
            {
                return null;
            }
        }

        public T Update(T entidad)
        {
            try
            {
                return Collection().ReplaceOne(e => e.Id == entidad.Id, entidad).ModifiedCount == 1 ? entidad : null;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
