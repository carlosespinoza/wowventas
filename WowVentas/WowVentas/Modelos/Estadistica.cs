﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WowVentas.Modelos
{
    public class Estadistica
    {
        public int Numero { get; set; }
        public string Producto { get; set; }
        public int Cantidad { get; set; }
        public float Monto { get; set; }
        public override string ToString()
        {
            return string.Format("[{0}] {1} {2} ${3}", Numero, Producto, Cantidad, Monto);
        }
    }
}
