﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WowVentas.Entidades
{
    public class Venta:BaseDTO
    {
        public Venta()
        {
            Items = new List<ItemVenta>();
        }
        public string Vendedor { get; set; }
        public string Cliente { get; set; }
        public List<ItemVenta> Items { get; set; }
        public float Total
        {
            get
            {
                return Items.Sum(p=>p.Cantidad*p.Producto.Costo);
            }
        }
        
    }
}
