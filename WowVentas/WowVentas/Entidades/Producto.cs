﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WowVentas.Entidades
{
    public class Producto:BaseDTO
    {
        public string Nombre { get; set; }
        public string Categoria { get; set; }
        public float Costo { get; set; }
        public override string ToString()
        {
            return string.Format("{0} [{1}] ${2}", Nombre, Categoria, Costo);
        }
    }
}
